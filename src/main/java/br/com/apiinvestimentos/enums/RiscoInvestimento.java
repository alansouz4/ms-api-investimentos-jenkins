package br.com.apiinvestimentos.enums;

public enum RiscoInvestimento {

    ALTO, BAIXO, MEDIO;
}
