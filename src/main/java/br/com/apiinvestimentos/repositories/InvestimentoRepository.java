package br.com.apiinvestimentos.repositories;

import br.com.apiinvestimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
