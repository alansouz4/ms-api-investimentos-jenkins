package br.com.apiinvestimentos.models;

public class SimulacaoResposta {

    private Double resultadoSimulacao;

    public SimulacaoResposta(Double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }

    public SimulacaoResposta() {
    }

    public Double getResultadoSimulacao() {
        return resultadoSimulacao;
    }

    public void setResultadoSimulacao(Double resultadoSimulacao) {
        this.resultadoSimulacao = resultadoSimulacao;
    }
}
